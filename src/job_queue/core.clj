(ns job-queue.core
  (:require [cheshire.core :as json]
            [clojure.pprint :refer [pprint]]
            [job-queue.queue :as queue])
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (let [raw-in-json (line-seq (java.io.BufferedReader. *in*))
        in-json (reduce str raw-in-json)
        jobs-info (queue/prepare in-json)]
      (println (queue/assign-jobs jobs-info))))
