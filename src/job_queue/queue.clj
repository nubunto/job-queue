(ns job-queue.queue
    (:use [clojure.data.priority-map]))

(defn- prioritize
    [jobs]
    (reduce
        (fn [pm job]
            (assoc pm (:id job) job))
        (priority-map-keyfn (comp not :urgent))
        jobs))

(defn prepare
    [requests]
    {:agents (->> requests
                  (filter :new_agent)
                  (map :new_agent))
     :jobs (->> requests
            (filter :new_job)
            (map :new_job)
            prioritize)
     :job-requests (->> requests
                          (filter :job_request)
                          (map :job_request))})

(defn- ensure-consistent
    [assigned-jobs]
    (let [job-ids (set (map #(get-in % [:job_assigned :job_id]) assigned-jobs))
          agent-ids (set (map #(get-in % [:job_assigned :agent_id]) assigned-jobs))
          aggregate (zipmap job-ids agent-ids)]
        (map (fn [[job agent]] {:job_assigned {:job_id job :agent_id agent}}) aggregate)))

(defn assign-jobs
    [{:keys [job-requests agents jobs]}]
    (let [assigned-jobs (for [[job-id job] jobs
                              agent agents
                              :when (and (some #(= (:id agent) (:agent_id %)) job-requests)
                                         (or (contains? (set (:primary_skillset agent)) (:type job))
                                             (contains? (set (:secondary_skillset agent)) (:type job))))]
                            {:job_assigned {:job_id job-id :agent_id (:id agent)}})]
        (ensure-consistent assigned-jobs)))

