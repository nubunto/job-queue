(ns job-queue.queue-test
    (:require [clojure.test :refer :all]
              [cheshire.core :as json]
              [job-queue.queue :as queue]))

(deftest queue-test
    (testing "empty agents and job requests should return empty assigned jobs"
        (let [q (queue/prepare [])
              assigned (queue/assign-jobs q)]
            (is (= [] assigned))))

    (testing "an agent that has not completed a previous job cannot have an assigned job"
        (let [info [{:new_agent {:id "bad-guy-1" :name "Darth Vader" :primary_skillset ["evil-questions"]}}
                    {:new_job {:id "evil-job-1" :type "evil-questions" :urgent true}}
                    {:new_job {:id "good-job-2" :type "good-questions" :urgent false}}]
              q (queue/prepare info)
              assigned (queue/assign-jobs q)]
            (is (= 0 (count assigned)))
            (is (= [] assigned))))

    (testing "an agent should only have one job assigned"
        (let [info [{:new_agent {:id "bad-guy-2" :name "The Emperor" :primary_skillset ["galactic-domination"]}}
                    {:new_job {:id "evil-job-2" :type "galactic-domination"}}
                    {:new_job {:id "evil-job-3" :type "galactic-domination"}}
                    {:job_request {:agent_id "bad-guy-2"}}]
              q (queue/prepare info)
              assigned (queue/assign-jobs q)]
            (is (= 1 (count assigned)))
            (is (= "bad-guy-2" (-> assigned (nth 0) :job_assigned :agent_id)))))

    (testing "sample output should match given the sample input"
        (let [sample-input (json/parse-string (slurp "resources/sample-input.json") true)
              actual-output (queue/assign-jobs (queue/prepare sample-input))
              expected-output (json/parse-string (slurp "resources/sample-output.json") true)]
            ;; need to reverse the expected output since it is ordered differently
            (is (= (reverse actual-output) expected-output)))))
