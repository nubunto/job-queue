# job-queue
[![pipeline status](https://gitlab.com/nubunto/job-queue/badges/master/pipeline.svg)](https://gitlab.com/nubunto/job-queue/commits/master)

Implementation of the concept of a job queue.

Given an input describing a possible job system, its agents and job requests, it outputs job assignment to agents.

## Usage

FIXME: explanation

    $ java -jar job-queue-0.1.0-standalone.jar [args]